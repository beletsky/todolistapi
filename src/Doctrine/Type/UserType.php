<?php

declare(strict_types=1);

namespace App\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use Lexik\Bundle\JWTAuthenticationBundle\Tests\Stubs\JWTUser;
use Symfony\Component\Security\Core\User\UserInterface;

class UserType extends Type
{
    public const NAME = 'user';
    private const DEFAULT_LENGTH = 30;

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return sprintf('VARCHAR(%s)', $fieldDeclaration['length'] ?? static::DEFAULT_LENGTH);
    }

    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * @param string $value
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): UserInterface
    {
        return new JWTUser($value);
    }

    /**
     * @param UserInterface $value
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): string
    {
        return $value->getUsername();
    }
}
