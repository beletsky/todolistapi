<?php

declare(strict_types=1);

namespace App\Entity;

use DateTimeInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class Task
{
    private int $id;
    private UserInterface $user;
    private string $text = '';
    private DateTimeInterface $date;
    private bool $completed = false;

    public function __construct(UserInterface $user, DateTimeInterface $date, string $text)
    {
        $this->user = $user;
        $this->date = $date;
        $this->text = $text;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setCompleted(bool $completed)
    {
        $this->completed = $completed;
    }

    public function getUser(): UserInterface
    {
        return $this->user;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getDate(): DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(DateTimeInterface $date): void
    {
        $this->date = $date;
    }
}
