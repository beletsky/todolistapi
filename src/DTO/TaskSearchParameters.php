<?php

declare(strict_types=1);

namespace App\DTO;

use DateTimeInterface;

class TaskSearchParameters
{
    private DateTimeInterface $endDate;
    private DateTimeInterface $startDate;

    public function __construct(DateTimeInterface $startDate, DateTimeInterface $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function getEndDate(): DateTimeInterface
    {
        return $this->endDate;
    }

    public function getStartDate(): DateTimeInterface
    {
        return $this->startDate;
    }
}
