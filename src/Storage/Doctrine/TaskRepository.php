<?php

namespace App\Storage\Doctrine;

use App\Doctrine\Type\UserType;
use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

class TaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    public function findOneByIdForUser(UserInterface $user, int $id): ?Task
    {
        $qb = $this->createQueryBuilder('t');
        $expr = $qb->expr();

        return $qb->andWhere($expr->eq('t.user', ':user'))
            ->andWhere($expr->eq('t.id', ':id'))
            ->setParameter('id', $id)
            ->setParameter('user', $user, UserType::NAME)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllForUserInDateRange(
        UserInterface $user,
        \DateTimeInterface $startDate,
        \DateTimeInterface $endDate
    ): iterable {
        return $this->findAllForUserInDateRangeQueryBuilder(
            $user,
            $startDate,
            $endDate
        )->getQuery()->getResult();
    }

    public function countAllForUserInDateRange(
        UserInterface $user,
        \DateTimeInterface $startDate,
        \DateTimeInterface $endDate
    ): int {
        return $this->findAllForUserInDateRangeQueryBuilder(
            $user,
            $startDate,
            $endDate
        )->select('count(t.id)')->getQuery()->getSingleScalarResult();
    }

    private function findAllForUserInDateRangeQueryBuilder(
        UserInterface $user,
        \DateTimeInterface $startDate,
        \DateTimeInterface $endDate
    ): QueryBuilder {
        $qb = $this->createQueryBuilder('t');
        $expr = $qb->expr();

        return $qb
            ->andWhere($expr->eq('t.user', ':user'))
            ->andWhere($expr->between('t.date', ':startDate', ':endDate'))
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->setParameter('user', $user, UserType::NAME);
    }
}
