<?php

namespace App\Storage\Doctrine;

use App\DTO\TaskSearchParameters;
use App\Entity\Task;
use App\Service\UserFetcher;
use App\Storage\TaskStorageInterface;
use Doctrine\ORM\EntityManagerInterface;

class UserTaskStorage implements TaskStorageInterface
{
    private TaskRepository $repository;

    private EntityManagerInterface $manager;

    private UserFetcher $userFetcher;

    public function __construct(EntityManagerInterface $manager, TaskRepository $repository, UserFetcher $userFetcher)
    {
        $this->repository = $repository;
        $this->manager = $manager;
        $this->userFetcher = $userFetcher;
    }

    public function getById(int $id): ?Task
    {
        return $this->repository->findOneByIdForUser($this->userFetcher->getUserLoggedIn(), $id);
    }

    public function find(TaskSearchParameters $searchParameters): iterable
    {
        return $this->repository->findAllForUserInDateRange(
            $this->userFetcher->getUserLoggedIn(),
            $searchParameters->getStartDate(),
            $searchParameters->getEndDate()
        );
    }

    public function count(TaskSearchParameters $searchParameters): int
    {
        return $this->repository->countAllForUserInDateRange(
            $this->userFetcher->getUserLoggedIn(),
            $searchParameters->getStartDate(),
            $searchParameters->getEndDate()
        );
    }

    public function save(Task $task): void
    {
        $this->manager->persist($task);
        $this->manager->flush();
    }
}
