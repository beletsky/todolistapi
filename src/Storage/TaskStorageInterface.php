<?php

namespace App\Storage;

use App\DTO\TaskSearchParameters;
use App\Entity\Task;

interface TaskStorageInterface
{
    public function getById(int $id): ?Task;

    public function find(TaskSearchParameters $searchParameters): iterable;

    public function count(TaskSearchParameters $searchParameters): int;

    public function save(Task $task): void;
}
