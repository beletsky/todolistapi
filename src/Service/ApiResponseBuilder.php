<?php

declare(strict_types=1);

namespace App\Service;

use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiResponseBuilder
{
    private int $code = 200;

    private array $errorMessages = [];

    private array $responseFields = ['status' => 'success'];

    private array $headers = [];

    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function setCode(int $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function addHeader(string $name, string $value): self
    {
        $this->headers[$name] = $value;

        return $this;
    }

    public function addErrorMessage(string $errorMessage): self
    {
        $this->errorMessages[] = $errorMessage;

        return $this;
    }

    public function addErrorMessages(array $errorMessages): self
    {
        $this->errorMessages = array_merge($this->errorMessages, $errorMessages);

        return $this;
    }

    public function addResponseField(string $name, $value): self
    {
        $this->responseFields[$name] = $value;

        return $this;
    }

    public function buildSuccessResponse(): JsonResponse
    {
        return $this->buildResponse();
    }

    public function buildErrorResponse(): JsonResponse
    {
        $this->responseFields = [
            'status' => 'error',
            'errors' => $this->errorMessages,
        ];

        return $this->buildResponse();
    }

    private function buildResponse(): JsonResponse
    {
        $this->responseFields['code'] = $this->code;

        return new JsonResponse(
            $this->serializer->serialize($this->responseFields, 'json'),
            $this->code,
            $this->headers,
            true
        );
    }
}
