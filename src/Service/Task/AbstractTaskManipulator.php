<?php

declare(strict_types=1);

namespace App\Service\Task;

use App\Entity\Task;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AbstractTaskManipulator
{
    protected ValidatorInterface $validator;
    protected SerializerInterface $serializer;
    protected array $errors = [];

    public function __construct(
        ValidatorInterface $validator,
        SerializerInterface $serializer
    ) {
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    protected function validate(Task $task): bool
    {
        $errors = $this->validator->validate($task);

        if (count($errors) > 0) {
            $this->errors = iterator_to_array($errors);

            return false;
        }

        return true;
    }
}
