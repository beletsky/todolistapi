<?php

declare(strict_types=1);

namespace App\Service\Task;

use App\Entity\Task;
use App\Exception\InvalidParameterFormatException;
use DateTime;
use Symfony\Component\HttpFoundation\Request;

class TaskUpdater extends AbstractTaskManipulator
{
    public function fromRequest(Task $task, Request $request): bool
    {
        $fields = $this->serializer->deserialize($request->getContent(), 'array', 'json');
        $this->updateText($task, $fields['text'] ?? null);
        $this->updateCompleted($task, $fields['completed'] ?? null);
        $this->updateDate($task, $fields['date'] ?? null);

        return $this->validate($task);
    }

    private function updateText(Task $task, ?string $text): void
    {
        if (!$text) {
            return;
        }

        $task->setText($text);
    }

    private function updateCompleted(Task $task, ?bool $completed): void
    {
        if (null === $completed) {
            return;
        }

        $task->setCompleted($completed);
    }

    private function updateDate(Task $task, ?string $date): void
    {
        if (!$date) {
            return;
        }

        $date = DateTime::createFromFormat('Y-m-d', $date);

        if (false === $date) {
            throw new InvalidParameterFormatException('date', 'Y-m-d');
        }

        $task->setDate($date);
    }
}
