<?php

declare(strict_types=1);

namespace App\Service\Task;

use App\DTO\TaskSearchParameters;
use App\Exception\InvalidParameterFormatException;
use App\Exception\MissingParameterException;
use DateTime;
use Symfony\Component\HttpFoundation\Request;

class TaskSearchParametersParser
{
    public function fromRequest(Request $request): TaskSearchParameters
    {
        $startDate = $this->getStartDate($request->get('startDate'));
        $endDate = $this->getEndDate($request->get('endDate'), $startDate);

        return new TaskSearchParameters($startDate, $endDate);
    }

    private function getStartDate(?string $startDateString): DateTime
    {
        if (!$startDateString) {
            throw new MissingParameterException('startDate');
        }

        $startDate = DateTime::createFromFormat('Y-m-d', $startDateString);

        if (false === $startDate) {
            throw new InvalidParameterFormatException('startDate', 'Y-m-d');
        }

        return $startDate->setTime(0, 0);
    }

    private function getEndDate(?string $endDateString, DateTime $default): DateTime
    {
        if (!$endDateString) {
            return $default;
        }

        $endDate = DateTime::createFromFormat('Y-m-d', $endDateString);

        if (false === $endDate) {
            throw new InvalidParameterFormatException('endDate', 'Y-m-d');
        }

        return $endDate;
    }
}
