<?php

declare(strict_types=1);

namespace App\Service\Task;

use App\Entity\Task;
use App\Service\UserFetcher;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TaskCreator extends AbstractTaskManipulator
{
    private UserFetcher $userFetcher;

    public function __construct(
        ValidatorInterface $validator,
        UserFetcher $userFetcher,
        SerializerInterface $serializer
    ) {
        $this->userFetcher = $userFetcher;

        parent::__construct($validator, $serializer);
    }

    public function fromRequest(Request $request): ?Task
    {
        $requestTask = $this->getRequestTask($request);

        if (!$requestTask) {
            return null;
        }

        return new Task(
            $this->userFetcher->getUserLoggedIn(),
            $requestTask->getDate(),
            $requestTask->getText()
        );
    }

    private function getRequestTask(Request $request): ?Task
    {
        $requestTask = $this->deserialize($request->getContent());
        $taskValid = $this->validate($requestTask);

        if ($taskValid) {
            return $requestTask;
        }

        return null;
    }

    private function deserialize(string $data): ?Task
    {
        return $this->serializer->deserialize(
            $data,
            Task::class,
            'json'
        );
    }
}
