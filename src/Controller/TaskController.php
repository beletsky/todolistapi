<?php

namespace App\Controller;

use App\Entity\Task;
use App\Service\ApiResponseBuilder;
use App\Service\Task\TaskCreator;
use App\Service\Task\TaskSearchParametersParser;
use App\Service\Task\TaskUpdater;
use App\Storage\TaskStorageInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class TaskController extends AbstractController
{
    private TaskStorageInterface $storage;
    private TaskSearchParametersParser $searchParametersParser;
    private ApiResponseBuilder $responseBuilder;

    public function __construct(
        TaskStorageInterface $storage,
        TaskSearchParametersParser $searchParametersParser,
        ApiResponseBuilder $responseBuilder
    ) {
        $this->storage = $storage;
        $this->searchParametersParser = $searchParametersParser;
        $this->responseBuilder = $responseBuilder;
    }

    /**
     * @SWG\Parameter(
     *     name="id",
     *     type="integer",
     *     in="path",
     *     description="Task identifier"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Return serialized task by given identifier",
     *     @SWG\Schema(
     *         type="object"
     *     )
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Not found code if task with given identifier not exist or can't be viewed"
     * )
     */
    public function show(int $id): JsonResponse
    {
        $task = $this->storage->getById($id);

        if (!$task) {
            return $this->responseBuilder
                ->setCode(404)
                ->addErrorMessage('Not found')
                ->buildErrorResponse();
        }

        return $this->responseBuilder
            ->addResponseField('task', $task)
            ->buildSuccessResponse();
    }

    /**
     * @SWG\Parameter(
     *     name="startDate",
     *     type="string",
     *     required=true,
     *     in="query",
     *     description="Start date in format Y-m-d for search in date range."
     * )
     * @SWG\Parameter(
     *     name="endDate",
     *     type="string",
     *     in="query",
     *     description="End date in format Y-m-d for search in date range. Equal to startDate if empty"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Return serialized task by given criteria",
     *     @SWG\Schema(
     *         type="array",
     *          @SWG\Items(ref=@Model(type=Task::class))
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns code 400 if search parameters not valid",
     *     examples={
     *         "Parameter startDate must follow format Y-m-d",
     *         "Parameter startDate is required"
     *     }
     * )
     */
    public function search(Request $request): JsonResponse
    {
        try {
            $searchParameters = $this->searchParametersParser->fromRequest($request);
        } catch (\Exception $e) {
            return $this->responseBuilder
                ->setCode(400)
                ->addErrorMessage($e->getMessage())
                ->buildErrorResponse();
        }

        $tasks = $this->storage->find($searchParameters);

        return $this->responseBuilder
            ->addResponseField('tasks', $tasks)
            ->buildSuccessResponse();
    }

    /**
     * @SWG\Parameter(
     *     name="startDate",
     *     type="string",
     *     required=true,
     *     in="query",
     *     description="Start date in format Y-m-d for search in date range."
     * )
     * @SWG\Parameter(
     *     name="endDate",
     *     type="string",
     *     in="query",
     *     description="End date in format Y-m-d for search in date range. Equal to startDate if empty"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Return number task by given criteria",
     *     @SWG\Schema(
     *         type="integer"
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns code 400 if search parameters not valid",
     *     examples={
     *         "Parameter startDate must follow format Y-m-d",
     *         "Parameter startDate is required"
     *     }
     * )
     */
    public function count(Request $request): JsonResponse
    {
        try {
            $searchParameters = $this->searchParametersParser->fromRequest($request);
        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage(), 400);
        }

        $count = $this->storage->count($searchParameters);

        return $this->responseBuilder
            ->addResponseField('count', $count)
            ->buildSuccessResponse();
    }

    /**
     * @SWG\Parameter(
     *     name="body",
     *     type="json",
     *     in="body",
     *     description="json task object",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/TaskCreate"),
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Return serialized task by given identifier",
     *     @SWG\Schema(
     *         type="object"
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns code 400 and errors array",
     *     @SWG\Schema(
     *         type="object"
     *     )
     * )
     */
    public function create(Request $request, TaskCreator $creator): JsonResponse
    {
        try {
            $task = $creator->fromRequest($request);

            if (!$task) {
                return $this->responseBuilder
                    ->setCode(400)
                    ->addErrorMessages($creator->getErrors())
                    ->buildErrorResponse();
            }

            $this->storage->save($task);

            return $this->responseBuilder
                ->addResponseField('task', $task)
                ->addResponseField('location', $this->generateUrl('api_task_show', ['id' => $task->getId()]))
                ->buildSuccessResponse();
        } catch (\Exception $e) {
            return $this->responseBuilder
                ->setCode(400)
                ->addErrorMessage($e->getMessage())
                ->buildErrorResponse();
        }
    }

    /**
     * @SWG\Parameter(
     *     name="body",
     *     type="json",
     *     in="body",
     *     description="json task object",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/TaskUpdate"),
     * )
     *
     * @SWG\Parameter(
     *     name="id",
     *     type="integer",
     *     in="path",
     *     description="Task identifier"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return serialized updated task by given identifier",
     *     @SWG\Schema(
     *         type="object"
     *     )
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Not found code if task with given identifier not exist or can't be viewed"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Returns code 400 and errors array",
     *     @SWG\Schema(
     *         type="object"
     *     )
     * )
     */
    public function update(Request $request, int $id, TaskUpdater $taskUpdater): JsonResponse
    {
        $task = $this->storage->getById($id);

        if (!$task) {
            return $this->responseBuilder
                ->setCode(404)
                ->addErrorMessage('Not found')
                ->buildErrorResponse();
        }

        $updated = $taskUpdater->fromRequest($task, $request);

        if (!$updated) {
            return $this->responseBuilder
                ->setCode(400)
                ->addErrorMessages($taskUpdater->getErrors())
                ->buildErrorResponse();
        }

        $this->storage->save($task);

        return $this->responseBuilder
            ->addResponseField('task', $task)
            ->addResponseField('location', $this->generateUrl('api_task_show', ['id' => $task->getId()]))
            ->buildSuccessResponse();
    }
}
