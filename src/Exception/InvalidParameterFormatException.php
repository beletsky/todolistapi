<?php

declare(strict_types=1);

namespace App\Exception;

class InvalidParameterFormatException extends \Exception
{
    public function __construct(string $parameterName, string $format, int $code = 0, \Throwable $previous = null)
    {
        parent::__construct(
            sprintf('Parameter %s must follow format %s', $parameterName, $format),
            $code,
            $previous
        );
    }
}
