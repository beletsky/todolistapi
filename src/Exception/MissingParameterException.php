<?php

declare(strict_types=1);

namespace App\Exception;

class MissingParameterException extends \Exception
{
    public function __construct(string $parameterName, int $code = 0, \Throwable $previous = null)
    {
        parent::__construct(
            sprintf('Parameter %s is required', $parameterName),
            $code,
            $previous
        );
    }
}
