<?php

namespace App\DataFixtures;

use App\Entity\Task;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Lexik\Bundle\JWTAuthenticationBundle\Security\User\JWTUser;

class AppFixtures extends Fixture
{
    private ObjectManager $manager;
    private Generator $faker;

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $this->faker = Factory::create();

        $this->createTasksForUser('test', new \DateTime('today'), 7);
        $this->createTasksForUser('test', new \DateTime('tomorrow'), 7);
        $this->createTasksForUser('test2', new \DateTime('today'), 7);
        $this->createTasksForUser('test2', new \DateTime('tomorrow'), 7);

        $manager->flush();
    }

    private function createTasksForUser(string $username, \DateTime $date, int $number): void
    {
        $user = new JWTUser($username);

        for ($i = 0; $i < $number; ++$i) {
            $task = new Task($user, $date, $this->faker->text(25));
            $this->manager->persist($task);
        }
    }
}
