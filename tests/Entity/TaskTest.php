<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Task;
use ReflectionClass;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\User\UserInterface;

class TaskTest extends KernelTestCase
{
    private Task $task;

    private string $text = 'Task text.';

    private int $id = 1;

    private string $date = '2020-05-02';

    public function setUp(): void
    {
        static::bootKernel();

        $userMock = $this->createMock(UserInterface::class);
        $today = new \DateTime($this->date);
        $task = new Task($userMock, $today, $this->text);
        $this->setTaskId($task, $this->id);

        $this->task = $task;
    }

    public function testSerialize(): void
    {
        $this->createMock(Task::class);

        $serializer = self::$container->get('jms_serializer');
        $stringSerialized = $serializer->serialize($this->task, 'json');
        $arrayDeserialized = $serializer->deserialize($stringSerialized, 'array', 'json');
        $this->assertSame([
            'id' => $this->id,
            'text' => $this->text,
            'date' => $this->date,
            'completed' => false,
        ], $arrayDeserialized);
    }

    private function setTaskId(Task $task, int $id): void
    {
        $taskReflectionClass = new ReflectionClass(Task::class);
        $idProperty = $taskReflectionClass->getProperty('id');
        $idProperty->setAccessible(true);
        $idProperty->setValue($task, $id);
    }
}
