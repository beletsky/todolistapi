# Usage
1. Run `cp .env.dist .env` and update variables you think need to be updated
1. Run this command to generate JWT private and public keys:
```
mkdir -p config/jwt \
&& echo "YOUR_PASSPHRASE" | openssl genpkey -pass stdin -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096 \
&& echo "YOUR_PASSPHRASE" | openssl pkey -in config/jwt/private.pem -passin stdin -out config/jwt/public.pem -pubout
``` 
1. Run `docker-compose up -d` to up containers
1. Run `docker exec -it todo-fpm php bin/console doctrine:migrations:migrate`
1. Run `docker exec -it todo-fpm php bin/console doctrine:fixtures:load` to populate db with tasks for users `test` and `test 2` for today and tomorrow
1. Run `docker exec -it todo-fpm bin/console lexik:jwt:generate-token test` (or `test2`) to get JWT
1. Run `docker exec -it todo-fpm bin/console assets:install`
1. Visit http://localhost 
1. Click Authorize
1. Copy/paste your token into field value without extra characters